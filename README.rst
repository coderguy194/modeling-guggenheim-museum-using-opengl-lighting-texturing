Created by `Md. Abdul Munim Dibosh` using *reStructuredText*

=========================================
 Modeling Guggenheim Museum using OpenGL
=========================================

Introduction
============
It was done as a part of the Computer Graphics course. Based on a Google Sketchup of the specified model the whole scene was developed and then texture and lighting was applied.

Level : Intermediate.

Dependency
===========
The project is compiled in *XCode*. If you don't have a Macbook/Mac PC then you can still use the source simply by putting them to your source folder in *Visual Studio* or *codeBlocks* project.

Open Source
===========
This is an Open Source project. If you could add some other interesting stuffs don't forget to shoot a `mail`_. I would love to know.

IF YOU ARE A BEGINNER IN OPENGL CHECK THIS `PROJECT`_.

Demo
====
.. image:: https://bitbucket.org/coderguy194/modeling-guggenheim-museum-using-opengl-lighting-texturing/raw/master/museum.gif


.. GENERAL LINKS

.. _`mail`: abdulmunim.buet@gmail.com
.. _`PROJECT`: https://bitbucket.org/coderguy194/iac-seminar-room-of-buet-by-opengl
