//
//  Beautifier.h
//  TexturingLighting
//
//  Created by Md. Abdul Munim Dibosh on 3/8/14.
//  Copyright (c) 2014 Md. Abdul Munim Dibosh. All rights reserved.
//

#ifndef __TexturingLighting__Beautifier__
#define __TexturingLighting__Beautifier__

#include <iostream>
#include "imports.h"
void drawText(char s[],int textSize,float posX,float posY);
#endif /* defined(__TexturingLighting__Beautifier__) */
