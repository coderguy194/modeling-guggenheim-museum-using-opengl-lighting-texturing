//
//  VectorHelper.h
//  TexturingLighting
//
//  Created by Md. Abdul Munim Dibosh on 3/8/14.
//  Copyright (c) 2014 Md. Abdul Munim Dibosh. All rights reserved.
//

#ifndef __TexturingLighting__VectorHelper__
#define __TexturingLighting__VectorHelper__

#include <iostream>
#include <math.h>

typedef struct{
    float x;
    float y;
    float z;
} Vertex;
typedef struct{
    int no_of_vertices;
    Vertex *vertices;
    
} Polygon;

extern Vertex Vertex3d(float x, float y, float z );
Polygon polygon(Vertex array[],int verticesCount);
extern Vertex getNormalOfAPolygonalSurface(Polygon p);

#endif /* defined(__TexturingLighting__VectorHelper__) */
