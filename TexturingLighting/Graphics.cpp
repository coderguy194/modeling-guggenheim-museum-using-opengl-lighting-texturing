#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<iostream>
#include<vector>
#include "camera.h"
#include "imageloader.h"
#include "imports.h"
#include "ImageHelper.h"
#include "Beautifier.h"

#define FALSE 0
#define BLACK 0.2, 0.2, 0.2
#define ShowUpvector

CCamera Camera;

using namespace std;
//make a global variable -- for tracking the anglular position of camera

//double cameraAngle; //in radian
double cameraAngleDelta;

double cameraHeight;
double cameraRadius;
double cameraX;
double cameraY;

GLuint  filter;                                 // Which Filter To Use
GLuint  texture[1];

double cameraAngle,cameraAngleVar,CLX,CLY,CLZ,CPX,CPY,CPZ;      //in radian
double rectAngle;
int opt = 1;
 //in degree

bool canDrawGrid;
//should show user manual or not
bool showHelp;

struct Point
{
   float x;
   float y;

};
//LIGHTING STUFFS
int light_color_choice = 1;//default
#define ORANGE 239/255.0,110/255.0,18/255.0
#define WHITE 0.9,0.9,0.9
#define CYAN 77.0/255.0,184.0/255.0,235.0/255.0
#define GREEN 0,1,0
#define RED 1,0,0
//COLOR OF LIGHT
const GLfloat light_ambient_white[]  = { WHITE, 1.0f };//day
const GLfloat light_ambient_cyan[]  = {CYAN, 1.0f };
const GLfloat light_ambient_cyan_light[]  = {CYAN, 0.5f };
const GLfloat light_ambient_orange[]  = {ORANGE, 1.0f };
const GLfloat light_ambient_green[]  = {GREEN, 1.0f };
const GLfloat light_ambient_red[]  = {RED, 1.0f };
const GLfloat light_ambient_black[]  = {BLACK, 1.0f };//night
//
float LightX=1,LightY=1,LightZ=0;
//LIGHT0
const GLfloat light_diffuse[]  = { 1.0f, 1.0f, 1.0f, 1.0f };
const GLfloat light_specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat light_position[] = {LightX,LightY,LightZ, 1.0f };
bool isNightMode = false;//night mode enabling
//LIGHT1 - global
bool globalLightOn = false;
const GLfloat light_diffuse2[]  = { 1.0f, 1.0f, 1.0f, 1.0f };
const GLfloat light_specular2[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat light_position2[] = { 1, 0, 1, 0.0f };//using 0.0f in last param so that it shines the same amount across our whole scene in a fixed direction
//material
const GLfloat mat_ambient[]    = { 0.7f, 0.7f, 0.7f, 1.0f };
const GLfloat mat_diffuse[]    = { 0.8f, 0.8f, 0.8f, 1.0f };
const GLfloat mat_specular[]   = { 1.0f, 1.0f, 1.0f, 1.0f };
const GLfloat high_shininess[] = { 100.0f };


GLuint _textureId; //The id of the texture
GLuint _textureIdWood;
GLuint _textureIdGlass;
GLuint _textureIdRoad;
GLuint _textureIdTest;

void initRendering() {
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);

}

void drawLine(float x1,float y1,float z1,float x2,float y2,float z2){

    glColor3f(0,0,0);
    glLineWidth(1.5);
    glBegin(GL_LINES);
    {
        glVertex3f(x1,y1,z1);
        glVertex3f(x2,y2,z2);
    }glEnd();
}


void drawQuad(Point x1, Point x2,Point x3,Point x4, float thickness,GLuint textureID);

vector<Point> drawHalfCircle(float radius)
{
        int num_segments = 12000;
        float cx=0,cy=0;
        vector<Point> path;

        glBegin(GL_POINTS);
        for(int ii = 0; ii < num_segments/2; ii++)
        {
                float theta = 2.0f * 3.1415926f * float(ii) / float(num_segments);//get the current angle
                float x = radius * cosf(theta);//calculate the x component
        float y = radius * sinf(theta);//calculate the y component
        glVertex3f(0,x + cx, y + cy);//output vertex
        Point point;
        point.x=x+cx;
        point.y=y+cy;
        path.push_back(point);
        }
    glEnd();

        return path;
}

void drawHalfCircleDimension(float radius,float thickness)
{
        int num_segments = 1200;
        float cx=0,cy=0;

        for(int ii = 0; ii < num_segments/2; ii++)
        {
                float theta = 2.0f * 3.1415926f * float(ii) / float(num_segments);//get the current angle
                float x = radius * cosf(theta);//calculate the x component
        float y = radius * sinf(theta);//calculate the y component

                glBegin(GL_LINES);
                        glVertex3f(0,x + cx, y + cy);
                        glVertex3f(thickness,x + cx, y + cy);
                glEnd();
        }
}

void fillBridgeShape(vector<Point> path, float height)
{
        //calculate normal
//        Vertex vertices1[4] = {Vertex3d(0,path[],0),Vertex3d(0,radius,radius*2+midDeviation),Vertex3d(0,y,radius*2+midDeviation),Vertex3d(0,y,0)};
//        Polygon p1 = polygon(vertices1,4);
//        Vertex normal1 = getNormalOfAPolygonalSurface(p1);
//        glNormal3f(normal1.x, normal1.y, normal1.z);
        for (vector<int>::size_type i = 0; i < path.size(); i++)
        {
                Point currentPoint = path[i];
                float distance = height - currentPoint.y;
                currentPoint.y=height + distance;
                glBegin(GL_LINES);
                {
                        glVertex3f(0,path[i].x,path[i].y);
                        glVertex3f(0,currentPoint.x,currentPoint.y);
                }
                glEnd();
        }
}

void drawBridgeShape(float radius)
{
        float midDeviation = 3;
        float thickness = 5;
        float sideFactor = 5;
        vector<Point> path = drawHalfCircle(radius);

        glColor3f(0,0,0);
        drawHalfCircleDimension(radius,thickness);
        glColor3f(1,1,1);

        glPushMatrix();
        glTranslatef(0,0,radius*2+midDeviation);
        glRotatef(180,1,0,0);
        glColor3f(0,0,0);
        drawHalfCircleDimension(radius,thickness);
        glColor3f(1,1,1);
        glPopMatrix();

        float height = radius+midDeviation/2;

        fillBridgeShape(path,height);

        glColor3f(1,0,0);
        glPushMatrix();
                glTranslatef(thickness,0,0);
                fillBridgeShape(path,height);
        glPopMatrix();

        glBegin(GL_QUADS);
        {

            float y = radius + sideFactor;

            //calculate normal
            Vertex vertices1[4] = {Vertex3d(0,radius,0),Vertex3d(0,radius,radius*2+midDeviation),Vertex3d(0,y,radius*2+midDeviation),Vertex3d(0,y,0)};
            Polygon p1 = polygon(vertices1,4);
            Vertex normal1 = getNormalOfAPolygonalSurface(p1);
            glNormal3f(normal1.x, normal1.y, normal1.z);
            
            glVertex3f(0,radius,0);
            glVertex3f(0,radius,radius*2+midDeviation);
            glVertex3f(0,y,radius*2+midDeviation);
            glVertex3f(0,y,0);

            //calculate normal
            Vertex vertices2[4] = {Vertex3d(thickness,radius,0),Vertex3d(thickness,radius,radius*2+midDeviation),Vertex3d(thickness,y,radius*2+midDeviation),Vertex3d(thickness,y,0)};
            Polygon p2 = polygon(vertices2,4);
            Vertex normal2 = getNormalOfAPolygonalSurface(p2);
            glNormal3f(normal2.x, normal2.y, normal2.z);
            
            glVertex3f(thickness,radius,0);
            glVertex3f(thickness,radius,radius*2+midDeviation);
            glVertex3f(thickness,y,radius*2+midDeviation);
            glVertex3f(thickness,y,0);

            //calculate normal
            Vertex vertices3[4] = {Vertex3d(thickness,y,0),Vertex3d(0,y,0),Vertex3d(0,y,radius*2+midDeviation),Vertex3d(thickness,y,radius*2+midDeviation)};
            Polygon p3 = polygon(vertices3,4);
            Vertex normal3 = getNormalOfAPolygonalSurface(p3);
            glNormal3f(normal3.x, normal3.y, normal3.z);
            
            glVertex3f(thickness,y,0);
            glVertex3f(0,y,0);
            glVertex3f(0,y,radius*2+midDeviation);
            glVertex3f(thickness,y,radius*2+midDeviation);

            y = -radius - sideFactor;

            
            //calculate normal
            Vertex vertices4[4] = {Vertex3d(0,-radius,0),Vertex3d(0,-radius,radius*2+midDeviation),Vertex3d(0,y,radius*2+midDeviation),Vertex3d(0,y,0)};
            Polygon p4 = polygon(vertices4,4);
            Vertex normal4 = getNormalOfAPolygonalSurface(p4);
            glNormal3f(normal4.x, normal4.y, normal4.z);
            
            glVertex3f(0,-radius,0);
            glVertex3f(0,-radius,radius*2+midDeviation);
            glVertex3f(0,y,radius*2+midDeviation);
            glVertex3f(0,y,0);

            //calculate normal
            Vertex vertices5[4] = {Vertex3d(thickness,-radius,0),Vertex3d(thickness,-radius,radius*2+midDeviation),Vertex3d(thickness,y,radius*2+midDeviation),Vertex3d(thickness,y,0)};
            Polygon p5 = polygon(vertices5,4);
            Vertex normal5 = getNormalOfAPolygonalSurface(p5);
            glNormal3f(normal5.x, normal5.y, normal5.z);
            
            glVertex3f(thickness,-radius,0);
            glVertex3f(thickness,-radius,radius*2+midDeviation);
            glVertex3f(thickness,y,radius*2+midDeviation);
            glVertex3f(thickness,y,0);

            //calculate normal
            Vertex vertices6[4] = {Vertex3d(thickness,y,0),Vertex3d(0,y,0),Vertex3d(0,y,radius*2+midDeviation),Vertex3d(thickness,y,radius*2+midDeviation)};
            Polygon p6 = polygon(vertices6,4);
            Vertex normal6 = getNormalOfAPolygonalSurface(p6);
            glNormal3f(normal6.x, normal6.y, normal6.z);
            
            glVertex3f(thickness,y,0);
            glVertex3f(0,y,0);
            glVertex3f(0,y,radius*2+midDeviation);
            glVertex3f(thickness,y,radius*2+midDeviation);

        }glEnd();
}

vector<Point> drawFloatingBezier(float startX, float startY, float endX, float endY, float bezierX, float bezierY, float height, float r, float g, float b)
{
    glColor3f(0,0,0);
    vector<Point> points;


        glBegin(GL_LINE_STRIP);{

            for(double t=0.0;t<=1;t+=0.1)
            {
                float x =  (  (1-t)*(1-t)*startX + 2*(1-t)*t*bezierX+t*t*endX);
                float y =  (  (1-t)*(1-t)*startY + 2*(1-t)*t*bezierY+t*t*endY);
                glVertex3f(x,y,height);

                Point point;
                point.x=x;
                point.y=y;
                points.push_back(point);
            }

        } glEnd();

    return points;

}


vector<Point> drawBezierLoop(float startX, float startY, float endX, float endY, float bezierX, float bezierY, float thickness)
{
    //glColor3f(0.3,0.3,0.3);
    vector<Point> points;

    for(float i=0;i<=thickness;i+=.1)
    {
        glBegin(GL_LINE_STRIP);{

            for(double t=0.0;t<=1;t+=0.1)
            {
                float x =  (  (1-t)*(1-t)*startX + 2*(1-t)*t*bezierX+t*t*endX);
                float y =  (  (1-t)*(1-t)*startY + 2*(1-t)*t*bezierY+t*t*endY);
                if(i==0||i==thickness){
                    glLineWidth(2);
                    glColor3f(0,0,0);
                    glVertex3f(x,y,i);
                }
                else{
                    glColor3f(.3,.3,.3);
                    glVertex3f(x,y,i);
                }


                Point point;
                point.x=x;
                point.y=y;
                points.push_back(point);
            }

        } glEnd();
    }
    return points;

}

void drawComplexFloatingBezier(float start1X, float start1Y, float end1X, float end1Y, float bezier1X, float bezier1Y,float start2X, float start2Y, float end2X, float end2Y, float bezier2X, float bezier2Y,float height1, float height2,float r, float g, float b, GLuint textureID){

    vector<Point> front_part;
    vector<Point> rear_part;

    front_part=drawFloatingBezier(start1X, start1Y, end1X, end1Y, bezier1X, bezier1Y, height1,r,g,b);
    rear_part=drawFloatingBezier(start2X, start2Y, end2X, end2Y, bezier2X, bezier2Y,height2,r,g,b);

    glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, textureID);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glColor3f(1.0f, 1.0f, 1.0f);

    for(int i=0;i<10;i++)
    {
       glBegin(GL_QUADS);
       {
           //calculate normal
           Vertex vertices[4] = {Vertex3d(front_part[i].x,front_part[i].y,height1),Vertex3d(front_part[i+1].x,front_part[i+1].y,height1),Vertex3d(rear_part[i+1].x,rear_part[i+1].y,height2),Vertex3d(rear_part[i].x,rear_part[i].y,height2)};
           Polygon p = polygon(vertices,4);
           Vertex normal = getNormalOfAPolygonalSurface(p);
           glNormal3f(normal.x, normal.y, normal.z);
           
           glTexCoord2f(0.0f, 0.0f);
           glVertex3f(front_part[i].x,front_part[i].y,height1);
           glTexCoord2f(6.0f, 0.0f);
           glVertex3f(front_part[i+1].x,front_part[i+1].y,height1);
           glTexCoord2f(6.0f, 6.0f);
           glVertex3f(rear_part[i+1].x,rear_part[i+1].y,height2);
           glTexCoord2f(0.0f, 6.0f);
           glVertex3f(rear_part[i].x,rear_part[i].y,height2);
       }glEnd();
    }
    glDisable(GL_TEXTURE_2D);

    drawLine(start1X,start1Y,height1,start2X,start2Y,height2);
    drawLine(end1X,end1Y,height1,end2X,end2Y,height2);
}

void drawComplexBezier(float start1X, float start1Y, float end1X, float end1Y, float bezier1X, float bezier1Y,float start2X, float start2Y, float end2X, float end2Y, float bezier2X, float bezier2Y,float thickness,float r, float g, float b,GLuint textureID){

    /*vector<Point> front_part;
    vector<Point> rear_part;

    front_part=drawBezierLoop(start1X, start1Y, end1X, end1Y, bezier1X, bezier1Y, thickness);
    rear_part=drawBezierLoop(start2X, start2Y, end2X, end2Y, bezier2X, bezier2Y, thickness);

    glColor3f(r,g,b);

    for(int i=0;i<10;i++)
       drawQuad(front_part[i],front_part[i+1],rear_part[i+1],rear_part[i],thickness,textureID);*/

    drawComplexFloatingBezier(start1X,start1Y,end1X,end1Y,bezier1X,bezier1Y,start2X,start2Y,end2X,end2Y,bezier2X,bezier2Y,thickness,thickness,r,g,b,textureID);
    drawComplexFloatingBezier(start1X,start1Y,end1X,end1Y,bezier1X,bezier1Y,start2X,start2Y,end2X,end2Y,bezier2X,bezier2Y,0,0,r,g,b,textureID);
    drawComplexFloatingBezier(start1X,start1Y,end1X,end1Y,bezier1X,bezier1Y,start1X,start1Y,end1X,end1Y,bezier1X,bezier1Y,thickness,0,r,g,b,textureID);
    drawComplexFloatingBezier(start2X,start2Y,end2X,end2Y,bezier2X,bezier2Y,start2X,start2Y,end2X,end2Y,bezier2X,bezier2Y,thickness,0,r,g,b,textureID);

    glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, textureID);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glColor3f(1.0f, 1.0f, 1.0f);
    glBegin(GL_QUADS);
    {
        //calculate normal
        Vertex vertices[4] = {Vertex3d(start1X,start1Y,0),Vertex3d(start2X,start2Y,0),Vertex3d(start2X,start2Y,thickness),Vertex3d(start1X,start1Y,thickness)};
        Polygon p = polygon(vertices,4);
        Vertex normal = getNormalOfAPolygonalSurface(p);
        glNormal3f(normal.x, normal.y, normal.z);
        
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(start1X,start1Y,0);
        glTexCoord2f(6.0f, 0.0f);
        glVertex3f(start2X,start2Y,0);
        glTexCoord2f(6.0f, 6.0f);
        glVertex3f(start2X,start2Y,thickness);
        glTexCoord2f(0.0f, 6.0f);
        glVertex3f(start1X,start1Y,thickness);
        
        //calculate normal
        Vertex vertices2[4] = {Vertex3d(end1X,end1Y,0),Vertex3d(end2X,end2Y,0),Vertex3d(end2X,end2Y,thickness),Vertex3d(end1X,end1Y,thickness)};
        Polygon p2 = polygon(vertices2,4);
        Vertex normal2 = getNormalOfAPolygonalSurface(p2);
        glNormal3f(normal2.x, normal2.y, normal2.z);

        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(end1X,end1Y,0);
        glTexCoord2f(6.0f, 0.0f);
        glVertex3f(end2X,end2Y,0);
        glTexCoord2f(6.0f, 6.0f);
        glVertex3f(end2X,end2Y,thickness);
        glTexCoord2f(0.0f, 6.0f);
        glVertex3f(end1X,end1Y,thickness);

    }glEnd();
    glDisable(GL_TEXTURE_2D);

}



void drawQuad(Point x1, Point x2,Point x3,Point x4, float thickness,GLuint textureID){

    glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, textureID);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glColor3f(1.0f, 1.0f, 1.0f);
    glBegin(GL_QUADS);
    {
        //calculate normal
        Vertex vertices[4] = {Vertex3d(x1.x,x1.y,thickness),Vertex3d(x2.x,x2.y,thickness),Vertex3d(x3.x,x3.y,thickness),Vertex3d(x4.x,x4.y,thickness)};
        Polygon p = polygon(vertices,4);
        Vertex normal = getNormalOfAPolygonalSurface(p);
        glNormal3f(normal.x, normal.y, normal.z);
        
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(x1.x,x1.y,thickness);
        glTexCoord2f(6.0f, 0.0f);
        glVertex3f(x2.x,x2.y,thickness);
        glTexCoord2f(6.0f, 6.0f);
        glVertex3f(x3.x,x3.y,thickness);
        glTexCoord2f(0.0f, 6.0f);
        glVertex3f(x4.x,x4.y,thickness);

    }glEnd();
    glDisable(GL_TEXTURE_2D);
}





void drawQuad(float x1, float y1, float z1,float x2, float y2, float z2,float x3, float y3, float z3,float x4, float y4, float z4, float r, float g, float b, GLuint textureID){

    //glColor3f(r,g,b);
    glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, textureID);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glColor3f(1.0f, 1.0f, 1.0f);
    glBegin(GL_QUADS);
    {
        //calculate normal
        Vertex vertices[4] = {Vertex3d(x1, y1, z1),Vertex3d(x2, y2, z2),Vertex3d(x3, y3,z3),Vertex3d(x4, y4,z4)};
        Polygon p = polygon(vertices,4);
        Vertex normal = getNormalOfAPolygonalSurface(p);
        glNormal3f(normal.x, normal.y, normal.z);
        
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(x1,y1,z1);
        glTexCoord2f(6.0f, 0.0f);
        glVertex3f(x2,y2,z2);
        glTexCoord2f(6.0f, 6.0f);
        glVertex3f(x3,y3,z3);
        glTexCoord2f(0.0f, 6.0f);
        glVertex3f(x4,y4,z4);

    }glEnd();
    glDisable(GL_TEXTURE_2D);

    drawLine(x1,y1,z1,x2,y2,z2);
    drawLine(x2,y2,z2,x3,y3,z3);
    drawLine(x3,y3,z3,x4,y4,z4);
    drawLine(x4,y4,z4,x1,y1,z1);
}


void draw3DQuad(float x1, float y1, float z1,float x2, float y2, float z2,float x3, float y3, float z3,float x4, float y4, float z4,float r, float g, float b, GLuint textureID){
//from topview left, right, righttop, lefttop point

    drawQuad(x1,y1,z1,x2,y2,z2,x3,y3,z3,x4,y4,z4,r,g,b,textureID);
    drawQuad(x1,y1,0,x2,y2,0,x3,y3,0,x4,y4,0,.3,.3,.3,textureID);
    drawQuad(x1,y1,0,x2,y2,0,x2,y2,z2,x1,y1,z1,.3,.3,.3,textureID);
    drawQuad(x4,y4,0,x3,y3,0,x3,y3,z3,x4,y4,z4,.3,.3,.3,textureID);
    drawQuad(x2,y2,0,x3,y3,0,x3,y3,z3,x2,y2,z2,.3,.3,.3,textureID);
    drawQuad(x4,y4,0,x1,y1,0,x1,y1,z1,x4,y4,z4,.3,.3,.3,textureID);

}

void draw3DQuadFloat(float x1, float y1, float z1,float x2, float y2, float z2,float x3, float y3, float z3,float x4, float y4, float z4,float height, float r, float g, float b,GLuint textureID){
//from topview left, right, righttop, lefttop point

    drawQuad(x1,y1,z1,x2,y2,z2,x3,y3,z3,x4,y4,z4,r,g,b,textureID);
    drawQuad(x1,y1,height,x2,y2,height,x3,y3,height,x4,y4,height,.3,.3,.3,textureID);
    drawQuad(x1,y1,height,x2,y2,height,x2,y2,z2,x1,y1,z1,.3,.3,.3,textureID);
    drawQuad(x4,y4,height,x3,y3,height,x3,y3,z3,x4,y4,z4,.3,.3,.3,textureID);
    drawQuad(x2,y2,height,x3,y3,height,x3,y3,z3,x2,y2,z2,.3,.3,.3,textureID);
    drawQuad(x4,y4,height,x1,y1,height,x1,y1,z1,x4,y4,z4,.3,.3,.3,textureID);

}

void drawNew3DQuadFloat(float x1, float y1, float z1,float x2, float y2, float z2,float x3, float y3, float z3,float x4, float y4, float z4,float height, float r, float g, float b,GLuint textureID){
//from topview left, right, righttop, lefttop point

    drawQuad(x1,y1,z1,x2,y2,z2,x3,y3,z3,x4,y4,z4,r,g,b,textureID);
    drawQuad(x1,y1,(z1-height),x2,y2,(z2-height),x3,y3,(z3-height),x4,y4,(z4-height),r,g,b,textureID);
    drawQuad(x1,y1,(z1-height),x2,y2,(z2-height),x2,y2,z2,x1,y1,z1,.3,.3,.3,textureID);
    drawQuad(x4,y4,(z4-height),x3,y3,(z3-height),x3,y3,z3,x4,y4,z4,.3,.3,.3,textureID);
    drawQuad(x2,y2,(z2-height),x3,y3,(z3-height),x3,y3,z3,x2,y2,z2,.3,.3,.3,textureID);
    drawQuad(x4,y4,(z4-height),x1,y1,(z1-height),x1,y1,z1,x4,y4,z4,.3,.3,.3,textureID);

}


void draw3DTriangle(float x1,float y1,float x2,float y2,float x3,float y3,float height, float r,float g, float b, GLuint textureID){

    drawQuad(x1,y1,0,x3,y3,0,x3,y3,height,x1,y1,height,.3,.3,.3,textureID);
    drawQuad(x3,y3,0,x2,y2,0,x2,y2,height,x3,y3,height,.3,.3,.3,textureID);
    drawQuad(x2,y2,0,x1,y1,0,x1,y1,height,x2,y2,height,.3,.3,.3,textureID);

    glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, textureID);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glColor3f(1.0f, 1.0f, 1.0f);
    glBegin(GL_TRIANGLES);
    {
        //calculate normal
        Vertex vertices[3] = {Vertex3d(x1, y1, 0),Vertex3d(x2, y2, 0),Vertex3d(x3, y3, 0)};
        Polygon p = polygon(vertices,3);
        Vertex normal = getNormalOfAPolygonalSurface(p);
        glNormal3f(normal.x, normal.y, normal.z);
        
        
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(x1,y1,0);
        glTexCoord2f(5.0f, 5.0f);
        glVertex3f(x2,y2,0);
        glTexCoord2f(5.0f, 0.0f);
        glVertex3f(x3,y3,0);
    }glEnd();


    glBegin(GL_TRIANGLES);
    {
        Vertex vertices[3] = {Vertex3d(x1, y1, height),Vertex3d(x2, y2, height),Vertex3d(x3, y3,height)};
        Polygon p = polygon(vertices,3);
        Vertex normal = getNormalOfAPolygonalSurface(p);
        glNormal3f(normal.x, normal.y, normal.z);
        
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(x1,y1,height);
        glTexCoord2f(5.0f, 0.0f);
        glVertex3f(x2,y2,height);
        glTexCoord2f(5.0f, 5.0f);
        glVertex3f(x3,y3,height);
    }glEnd();
    glDisable(GL_TEXTURE_2D);
}



void drawBoldLine(float x1,float y1,float z1,float x2,float y2,float z2){

    glColor3f(0,0,0);
    glLineWidth(3);
    glBegin(GL_LINES);
    {
        glVertex3f(x1,y1,z1);
        glVertex3f(x2,y2,z2);
    }glEnd();
}


//void drawScene() {
//	//glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
//
//	//glMatrixMode(GL_MODELVIEW);
//    //glLoadIdentity();
//
//	glTranslatef(0.0f, 1.0f, -6.0f);
//
//	glEnable(GL_TEXTURE_2D);
//	glBindTexture(GL_TEXTURE_2D, _textureId);
//
//	//Bottom
//	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
//	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
//	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
//	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
//	glColor3f(1.0f, 0.2f, 0.2f);
//	glBegin(GL_QUADS);
//
//	glNormal3f(0.0, 1.0f, 0.0f);
//	glTexCoord2f(0.0f, 0.0f);
//	glVertex3f(-2.5f, -2.5f, 2.5f);
//	glTexCoord2f(1.0f, 0.0f);
//	glVertex3f(2.5f, -2.5f, 2.5f);
//	glTexCoord2f(1.0f, 1.0f);
//	glVertex3f(2.5f, -2.5f, -2.5f);
//	glTexCoord2f(0.0f, 1.0f);
//	glVertex3f(-2.5f, -2.5f, -2.5f);
//
//	glEnd();
//
//	//Back
//	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
//	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
//	glColor3f(1.0f, 1.0f, 1.0f);
//	glBegin(GL_TRIANGLES);
//
//	glNormal3f(0.0f, 0.0f, 1.0f);
//	glTexCoord2f(0.0f, 0.0f);
//	glVertex3f(-2.5f, -2.5f, -2.5f);
//	glTexCoord2f(5.0f, 5.0f);
//	glVertex3f(0.0f, 2.5f, -2.5f);
//	glTexCoord2f(10.0f, 0.0f);
//	glVertex3f(2.5f, -2.5f, -2.5f);
//
//	glEnd();
//
//	//Left
//	glDisable(GL_TEXTURE_2D);
//	glColor3f(1.0f, 0.7f, 0.3f);
//	glBegin(GL_QUADS);
//
//	glNormal3f(1.0f, 0.0f, 0.0f);
//	glVertex3f(-2.5f, -2.5f, 2.5f);
//	glVertex3f(-2.5f, -2.5f, -2.5f);
//	glVertex3f(-2.5f, 2.5f, -2.5f);
//	glVertex3f(-2.5f, 2.5f, 2.5f);
//
//	glEnd();
//
//}


//*********************************************************************//


    void drawTriangle(float x1, float y1, float z1,float x2, float y2, float z2,float x3, float y3, float z3,float r, float g, float b,GLuint textureID){
        glEnable(GL_TEXTURE_2D);
        glBindTexture(GL_TEXTURE_2D, textureID);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glColor3f(1.0f, 1.0f, 1.0f);
        glBegin(GL_TRIANGLES);
        {
            //glColor3f(r,g,b);
            glTexCoord2f(0.0f, 0.0f);
            glVertex3f(x1,y1,z1);
            glTexCoord2f(5.0f, 5.0f);
            glVertex3f(x2,y2,z2);
            glTexCoord2f(5.0f, 0.0f);
            glVertex3f(x3,y3,z3);
        }glEnd();
        glDisable(GL_TEXTURE_2D);
    }



void totalScene(){

    draw3DQuad(-37.47,-2.69,12.34,-19.57,-2.69,12.34,-25.15,-66.29,12.34,-44.59,-66.29,12.34,0.82,0.71,0.5,_textureIdWood);
    drawLine(-37.47,-2.69,12.34,-19.57,-2.69,12.34);
    drawLine(-19.57,-2.69,12.34,-25.15,-66.29,12.34);
    drawLine(-25.15,-66.29,12.34,-44.59,-66.29,12.34);
    drawLine(-37.47,-2.69,12.34,-44.59,-66.29,12.34);

    drawLine(-37.47,-2.69,20.35,-19.57,-2.69,20.35);
    drawLine(-19.57,-2.69,20.35,-20.24,-10.32,20.35);

    draw3DQuad(-19.56,-55.84,4.34,-12.58,-60.62,4.34,-12.58,-50.94,4.34,-17.43,-48.25,4.34,0.82,0.71,0.5,_textureIdWood);
    draw3DQuad(-12.58,-55.84,4.34,-12.58,-50.94,4.34,-9.76,-52.51,4.34,-9.76,-55.84,4.34,0.82,0.71,0.5,_textureIdWood);
    draw3DQuad(-9.76,-52.51,3.28,-5.03,-52.51,3.28,-5.03,-50.36,3.28,-17.43,-48.25,3.28,0.82,0.71,0.5,_textureIdWood);
    drawComplexBezier(-24.95,-64.05,-23.5,-47.49,-23.5,-47.49,-20.72,-66.79,-17.43,-48.45,-20.32,-60.5,8.13,0.92,0.81,0.5,_textureIdWood);
    draw3DQuad(-20.20,-47.78,13.03,-0.35,-51.15,13.03,5.08,-31.16,13.03,-20.2,-31.16,13.03,0.616,0.686,0.702,_textureIdWood);
    drawComplexBezier(5.08,-31.16,-20.2,-31.16,-20.2,-31.16,5.08,-31.16,-20.2,-31.16,-7.56,-25.48,13.03,0.616,0.686,0.702,_textureIdWood);

    draw3DQuad(-23.5,-47.49,8.13,-10.75,-48.85,8.13,-9.76,-24.36,8.13,-21.47,-24.36,8.13,0.92,0.81,0.5,_textureIdWood);
    drawComplexBezier(1.51,-24.36,-21.47,-24.36,-21.47,-24.36,-0.87,-20.81,-20.24,-10.32,-13.66,-21.01,8.13,0.92,0.81,0.5,_textureIdWood);

    drawQuad(-48.53,-34.3,18.04,-41.36,-34.3,18.04,-40.12,-26.09,18.04,-47.2,-26.09,18.04,0.82,0.71,0.5,_textureId);
    drawQuad(-48.53,-34.3,18.04,-41.36,-34.3,18.04,-45.55,-34.3,12.34,-48.53,-34.3,12.34,.3,.3,.3,_textureId);
    drawQuad(-40.12,-26.09,18.04,-47.2,-26.09,18.04,-47.2,-26.09,12.34,-44.31,-26.09,12.34,.3,.3,.3,_textureId);
    drawQuad(-41.36,-34.3,18.04,-40.12,-26.09,18.04,-47.2,-26.09,12.34,-45.55,-34.3,12.34,.3,.3,.3,_textureId);

    draw3DQuad(-48.53,-34.3,18.04,-47.2,-26.09,18.04,-52.68,-23.04,18.04,-52.68,-33.24,18.04,0.82,0.71,0.5,_textureId);
    draw3DQuad(-52.68,-23.04,18.04,-52.68,-33.24,18.04,-58.46,-31.77,18.04,-56.25,-22.76,18.04,0.82,0.71,0.5,_textureId);
    draw3DTriangle(-52.68,-23.04,-56.25,-22.76,-55.77,-20.86,18.04,0.82,0.71,0.5,_textureId);

    drawComplexBezier(-47.2,-26.09,-52.68,-23.04,-52.68,-23.04,-44.38,-22.77,-52.68,-19.23,-47.23,-17.95,18.04,0.82,0.71,0.5,_textureId);
    drawComplexBezier(-56.25,-22.76,-58.46,-31.77,-58.46,-31.77,-56.25,-22.76,-58.46,-31.77,-58.46,-26.09,18.04,0.82,0.71,0.5,_textureId);
    drawComplexBezier(-58.46,-31.77,-59.53,-34.30,-56.02,-33.67,-48.53,-34.3,-48.53,-34.3,-48.53,-34.3,18.04,0.82,0.71,0.5,_textureId);
    drawComplexBezier(-59.17,-34.35,-48.53,-34.3,-48.53,-34.3,-59.17,-34.35,-48.53,-34.3,-54.25,-42.94,18.04,0.82,0.71,0.5,_textureId);
    drawComplexFloatingBezier(-41.36,-34.30,-40.12,-26.09,-40.12,-26.09,-41.36,-34.30,-40.12,-26.09,-39.82,-30.85,18.04,18.04,0.82,0.71,0.5,_textureId);


    draw3DTriangle(-34.44,-69.8,-20.03,-82.34,-51.31,-89.19,12.22,0,0,1,_textureId);
    drawComplexBezier(-34.44,-69.8,-20.03,-82.34,-20.03,-82.34,-34.44,-69.8,-20.03,-82.34,-24.22,-72.61,12.22,0,0,1,_textureId);
    drawComplexBezier(-20.03,-82.34,-51.31,-89.19,-51.31,-89.19,-20.03,-82.34,-51.31,-89.19,-37.72,-102.49,12.22,0,0,1,_textureId);


    drawComplexBezier(-58.45,-86.15,-48.41,-83.52,-48.41,-83.52,-58.45,-86.15,-48.41,-83.52,-58.45,-70.05,12.22,0,0,1,_textureId);
    drawComplexBezier(-58.45,-86.15,-48.41,-83.52,-48.41,-83.52,-56.68,-90.35,-46.87,-88.22,-52.53,-84.78,12.22,0,0,1,_textureId);

    draw3DQuad(-47.43,-6.22,12.34,-38.56,-12.42,12.34,-40.26,-31.84,12.34,-55.77,-20.86,12.34,0.92,0.81,0.5,_textureId);

    //draw3DQuad(-40.26,-31.84,12.34,-44.12,-29.11,12.34,-48.25,-49.65,12.34,-42.35,-46.28,12.34,1,1,1);

    drawComplexBezier(-40.26,-31.84,-59.53,-34.3,-59.53,-34.3,-42.35,-46.28,-64.6,-53.76,-53.71,-53.58,12.34,0.92,0.81,0.5,_textureId);
    draw3DTriangle(-40.26,-31.84,-59.53,-34.3,-54.35,-22.91,12.34,0.92,0.81,0.5,_textureId);

    draw3DTriangle(-59.53,-34.3,-58.5,-31.01,-65.12,-39.37,12.34,0.92,0.81,0.5,_textureId);

    drawTriangle(-64.6,-53.76,12.55,-68.09,-53.82,12.55,-65.32,-53.82,0,.3,.3,.3,_textureId);
    drawQuad(-59.53,-34.3,12.34,-65.12,-39.37,12.34,-68.09,-53.82,12.34,-64.6,-53.76,12.55,0.92,0.81,0.5,_textureId);
    drawTriangle(-65.12,-39.37,12.34,-65.12,-39.37,0,-68.09,-53.82,12.34,.3,.3,.3,_textureId);
    drawTriangle(-65.2,-53.82,0,-65.12,-39.37,0,-68.09,-53.82,12.34,.3,.3,.3,_textureId);
    drawLine(-68.09,-53.82,12.34,-65.12,-39.37,0);


    draw3DQuad(-63.4,22.08,10.19,-99.92,13.57,10.19,-95.52,-5.32,10.19,-59.33,3.12,10.19,0.82,0.71,0.5,_textureIdWood);
    drawLine(-63.4,22.08,10.19,-99.92,13.57,10.19);
    drawLine(-99.92,13.57,10.19,-95.52,-5.32,10.19);
    drawLine(-95.52,-5.32,10.19,-59.33,3.12,10.19);
    drawLine(-63.4,22.08,10.19,-59.33,3.12,10.19);

    draw3DQuad(-68.13,16.66,12.34,-94.43,10.53,12.34,-90.55,0.84,12.34,-65.79,6.61,12.34,0.82,0.71,0.5,_textureIdWood);
    drawLine(-68.13,16.66,12.34,-94.43,10.53,12.34);
    drawLine(-94.43,10.53,12.34,-90.55,0.84,12.34);
    drawLine(-90.55,0.84,12.34,-65.79,6.61,12.34);
    drawLine(-68.13,16.66,12.34,-65.79,6.61,12.34);


    draw3DTriangle(-80.28,22.08,-72.56,19.95,-94.86,14.75,8.44,0.82,0.71,0.5,_textureIdWood);
    drawComplexBezier(-101.78,33.08,-94.86,14.75,-94.86,14.75,-85.95,41.85,-80.28,22.08,-85.98,30.67,8.44,0.82,0.71,0.5,_textureIdWood);

    draw3DQuad(-69.99,0.71,10.19,-64.34,0.37,10.19,-70.39,-9.99,10.19,-76.72,-6.48,10.19,0.82,0.71,0.5,_textureIdWood);
    draw3DTriangle(-69.99,0.71,-76.72,-6.48,-73.19,-0.11,10.19,0.82,0.71,0.5,_textureIdWood);

    drawComplexBezier(-64.34,0.37,-70.39,-9.99,-70.39,-9.99,-64.34,0.37,-70.39,-9.99,-61.07,-8.05,10.19,0.82,0.71,0.5,_textureIdWood);

    drawComplexBezier(-92.12,-4.52,-92.12,-6.99,-88.9,-5.27,-73.19,-0.11,-73.19,-0.11,-73.19,-0.11,10.19,0.82,0.71,0.5,_textureIdWood);

    drawComplexBezier(-92.12,-6.99,-73.19,-0.11,-73.19,-0.11,-91.23,-19.3,-76.72,-6.49,-88.1,-8.23,10.19,0.82,0.71,0.5,_textureIdWood);
    draw3DTriangle(-99.18,-12.93,-91.23,-19.3,-92.12,-6.99,10.19,0.82,0.71,0.5,_textureIdWood);
    drawComplexBezier(-99.18,-12.93,-91.23,-19.3,-91.23,-19.3,-99.18,-12.93,-91.23,-19.3,-95.82,-16.88,10.19,0.82,0.71,0.5,_textureIdWood);

    drawTriangle(-69.66,0.71,18.94,-64.34,0.37,19.94,-59.33,3.12,21.31,0.753,0.753,0.753,_textureId);
    drawLine(-69.66,0.71,18.94,-64.34,0.37,19.94);
    drawLine(-64.34,0.37,19.94,-59.33,3.12,21.31);
    drawLine(-69.66,0.71,18.94,-59.33,3.12,21.31);

    drawTriangle(-64.34,0.37,19.94,-59.33,3.12,21.31,-45.13,4.48,16.8,0.753,0.753,0.753,_textureId);
    drawLine(-59.33,3.12,21.31,-45.13,4.48,16.8);
    drawLine(-64.34,0.37,19.94,-45.13,4.48,16.8);

    drawQuad(-69.66,0.71,18.94,-64.34,0.37,19.94,-64.34,0.37,10.19,-69.66,0.71,10.19,.3,.3,.3,_textureId);
    drawLine(-64.34,0.37,19.94,-64.34,0.37,10.19);
    drawLine(-64.34,0.37,10.19,-69.66,0.71,10.19);
    drawLine(-69.66,0.71,18.94,-69.66,0.71,10.19);

    drawTriangle(-64.34,0.37,19.94,-45.13,4.48,16.8,-64.34,0.37,0,0.753,0.753,0.753,_textureId);//_textureId
    drawLine(-45.13,4.48,16.8,-64.34,0.37,0);

    drawTriangle(-45.13,4.48,16.8,-64.34,0.37,0,-50.7,-0.5,0,0.753,0.753,0.753,_textureId);//_textureId
    drawLine(-45.13,4.48,16.8,-50.7,-0.5,0);

    drawTriangle(-45.13,4.48,16.8,-52.92,6,1.26,-50.7,-0.5,0,0.753,0.753,0.753,_textureId);//_textureId
    drawLine(-45.13,4.48,16.8,-52.92,6,1.26);
    drawLine(-52.92,6,1.26,-50.7,-0.5,0);

    drawTriangle(-52.92,6,1.26,-50.7,-0.5,0,-50.68,5.6,0,0.753,0.753,0.753,_textureId);//_textureId
    drawLine(-52.92,6,1.26,-50.68,5.6,0);

    drawQuad(-59.33,3.12,21.31,-69.66,0.71,18.94,-69.66,0.71,10.19,-59.33,3.12,10.19,.753,.753,.753,_textureId);
    drawLine(-59.33,3.12,21.31,-69.66,0.71,18.94);
    drawLine(-69.66,0.71,18.94,-69.66,0.71,10.19);
    drawLine(-69.66,0.71,10.19,-59.33,3.12,10.19);
    drawLine(-59.33,3.12,21.31,-59.33,3.12,10.19);

    drawQuad(-59.33,3.12,21.31,-59.33,3.12,10.19,-63.4,22.08,10.19,-63.4,22.08,23.04,.753,.753,.753,_textureId);
    drawLine(-59.33,3.12,10.19,-63.4,22.08,10.19);
    drawLine(-63.4,22.08,10.19,-63.4,22.08,23.04);
    drawLine(-59.33,3.12,21.31,-63.4,22.08,23.04);

    drawQuad(-63.24,22.73,19.36,-63.4,22.08,19.36,-63.4,22.08,23.04,-63.24,22.73,0,.753,.753,.753,_textureId);
    drawLine(-63.24,22.73,19.36,-63.4,22.08,19.36);
    drawLine(-63.4,22.08,19.36,-63.4,22.08,23.04);
    drawLine(-63.4,22.08,23.04,-63.24,22.73,0);
    drawLine(-63.24,22.73,19.36,-63.24,22.73,0);

    drawTriangle(-63.4,22.08,23.04,-63.24,22.73,23.04,-59.33,3.12,21.31,.753,.753,.753,_textureId);
    drawLine(-63.24,22.73,23.04,-59.33,3.12,21.31);
    drawLine(-63.4,22.08,23.04,-63.24,22.73,23.04);

    drawQuad(-63.24,22.73,23.04,-59.33,3.12,21.31,-57.97,11.5,23.04,-59.24,22.71,23.04,.753,.753,.753,_textureId);
    drawLine(-57.97,11.5,23.04,-59.24,22.71,23.04);

    drawTriangle(-59.33,3.12,21.31,-57.97,11.5,23.04,-57.3,9.67,23.04,.753,.753,.753,_textureId);
    drawLine(-59.33,3.12,21.31,-57.97,11.5,23.04);
    drawLine(-57.97,11.5,23.04,-57.3,9.67,23.04);
    drawLine(-59.33,3.12,21.31,-57.3,9.67,23.04);

    draw3DTriangle(-50.37,20.99,-58.15,13.45,-51,5.66,24.92,.3,.3,.3,_textureId);
    drawComplexBezier(-50.37,20.99,-58.15,13.45,-58.15,13.45,-50.37,20.99,-58.15,13.45,-56.99,19.99,24.92,.3,.3,.3,_textureId);
    drawComplexBezier(-58.15,13.45,-51,5.66,-58.15,13.45,-58.15,13.45,-51,5.66,-58.65,7.31,24.92,.3,.3,.3,_textureId);

    draw3DTriangle(-50.37,20.99,-49.05,5.84,-51,5.66,24.92,.3,.3,.3,_textureId);
    draw3DTriangle(-50.37,20.99,-49.05,5.84,-47.22,6.51,24.92,.3,.3,.3,_textureId);
    drawTriangle(-59.33,3.12,21.31,-57.3,9.67,23.04,-45.13,4.48,16.8,.753,.753,.753,_textureId);


    draw3DQuad(-63.24,22.73,19.36,-63.4,22.08,19.36,-72.56,19.95,19.36,-80.28,22.08,19.36,0.616,0.686,0.702,_textureId);
    drawLine(-72.56,19.95,19.36,-72.56,19.95,0);
    drawLine(-80.28,22.08,19.36,-80.28,22.08,0);

    draw3DQuad(-80.28,22.08,19.36,-63.24,22.73,19.36,-56.19,28.45,19.36,-56.05,40.65,19.36,0.616,0.686,0.702,_textureId);
    draw3DTriangle(-56.19,28.45,-56.05,40.65,-54.57,35.63,19.36,0.616,0.686,0.702,_textureId);

    drawComplexFloatingBezier(-80.28,22.08,-90.06,49.44,-90.06,33.36,-80.28,22.08,-85.95,41.85,-85.95,30.67,19.36,8.44,0,0,0,_textureId);

    drawQuad(-85.95,41.85,8.44,-90.06,49.44,19.36,-85.42,46,4.42,-85.95,41.85,4.42,0.616,0.686,0.702,_textureId);
    drawQuad(-85.42,46,4.42,-85.95,41.85,4.42,-85.95,41.85,0,-85.42,46,0,0.616,0.686,0.702,_textureId);

    drawComplexFloatingBezier(-80.28,22.08,-90.06,49.44,-90.06,33.36,-56.05,40.65,-90.06,49.44,-70.86,50.48,19.36,19.36,0,0,0,_textureId);
    drawComplexFloatingBezier(-56.05,40.65,-90.06,49.44,-70.86,50.48,-56.05,40.65,-85.42,46,-70.86,50.48,19.36,4.42,0,0,0,_textureId);
    drawComplexFloatingBezier(-56.05,40.65,-85.42,46,-70.86,50.48,-56.05,40.65,-85.42,46,-70.86,50.48,0,4.42,0,0,0,_textureId);



    draw3DQuad(-59.24,22.71,23.04,-54.57,35.63,23.04,-54.57,13.44,23.04,-58.14,13.44,23.04,.753,.753,.753,_textureId);
    draw3DQuad(-54.57,35.63,23.04,-54.57,13.44,23.04,-48.73,13.44,23.04,-52.95,33.33,23.04,.753,.753,.753,_textureId);

    draw3DTriangle(-63.24,22.73,-55.77,23.04,-55.9,31.06,25.1,.4,.624,.64,_textureId);


    drawComplexFloatingBezier(-55.95,31.06,-76.3,39.11,-61.39,47.12,-63.24,22.73,-76.3,39.11,-73.6,27.87,25.1,25.1,0,0,0,_textureId);
    drawComplexFloatingBezier(-63.24,22.73,-76.3,39.11,-73.6,27.87,-63.24,22.73,-76.3,31.87,-72.3,24.94,25.1,19.36,0,0,0,_textureId);

    drawComplexFloatingBezier(-55.95,31.06,-76.3,39.11,-61.39,47.12,-55.95,31.06,-67.58,38.99,-59.02,36.82,25.1,19.36,0,0,0,_textureId);
    drawComplexFloatingBezier(-76.3,39.11,-76.3,39.11,-76.3,39.11,-67.58,38.99,-76.3,31.87,-73.25,37.04,25.1,19.36,0,0,0,_textureId);


    draw3DTriangle(-47.22,6.51,-45.63,7.63,-37.47,-2.69,20.35,.753,.753,.753,_textureId);
    drawLine(-47.22,6.51,20.35,-45.63,7.63,20.35);
    drawLine(-45.63,7.63,20.35,-37.47,-2.69,20.35);
    drawLine(-47.22,6.51,20.35,-37.47,-2.69,20.35);

    draw3DQuad(-45.63,7.63,20.35,-37.47,-2.69,20.35,-44.95,14.4,20.35,-47.12,13.96,20.35,.753,.753,.753,_textureId);
    draw3DQuad(-44.95,14.4,20.35,-41.24,13.71,20.35,-24.49,2.14,20.35,-37.47,-2.69,20.35,.753,.753,.753,_textureId);
    draw3DTriangle(-24.49,2.14,-19.57,-2.69,-37.47,-2.69,20.35,.753,.753,.753,_textureId);
    draw3DTriangle(-24.49,2.14,-19.57,-2.69,-21.35,2.14,20.35,.753,.753,.753,_textureId);

    draw3DQuad(-19.57,-2.69,20.35,-21.35,2.14,20.35,-10.55,8.93,20.35,-20.24,-10.32,20.35,.753,.753,.753,_textureId);

    drawComplexBezier(-20.24,-10.32,8.63,-17.75,-10.71,-28.8,-10.55,8.93,8.63,-17.75,11.37,7.14,20.35,.753,.753,.753,_textureId);
    drawComplexBezier(-10.55,8.93,8.63,-17.75,11.37,7.14,8.98,6.48,11.8,-16.04,12.98,-5.72,23.04,.753,.753,.753,_textureId);

    drawComplexBezier(6.19,-19.12,11.8,-16.04,11.8,-16.04,6.19,-19.12,11.8,-16.04,10.08,-20.02,23.04,.753,.753,.753,_textureId);

    draw3DQuad(8.98,6.68,23.04,8.82,10.61,23.04,3.57,14.16,23.04,3.44,12.63,23.04,.753,.753,.753,_textureId);
    draw3DQuad(8.98,6.68,23.04,3.44,12.63,23.04,-12.04,14.74,23.04,-10.55,8.93,23.04,.753,.753,.753,_textureId);
    draw3DTriangle(-12.04,14.74,-15.09,19.14,-10.55,8.93,23.04,.753,.753,.753,_textureId);


    //Complex

    draw3DQuad(-41.24,13.71,33.88,-37.67,16,33.88,-33.89,16,33.88,-39.71,12.65,33.88,0.616,0.686,0.702,_textureId);
    draw3DTriangle(-39.71,12.65,-36.98,10.77,-27.14,19.89,33.88,0.616,0.686,0.702,_textureId);

    drawLine(-41.24,13.71,33.88,-37.67,16,33.88);
    drawLine(-37.67,16,33.88,-33.89,16,33.88);
    drawLine(-33.89,16,33.88,-27.14,19.89,33.88);
    drawLine(-41.24,13.71,33.88,-36.98,10.77,33.88);

    drawLine(-41.24,13.71,33.88,-41.24,13.71,20.35);
    drawLine(-36.98,10.77,33.88,-36.98,10.77,20.35);


    drawQuad(-27.14,19.89,33.88,-36.98,10.77,33.88,-32.3,6.7,33.88,-22.99,11.51,33.88,0.616,0.686,0.702,_textureId);
    drawLine(-27.14,19.89,33.88,-22.99,11.51,33.88);
    drawLine(-36.98,10.77,33.88,-32.3,6.7,33.88);

    drawQuad(-32.3,6.7,33.88,-22.99,11.51,33.88,-11.43,3.08,33.88,-21.29,-2.85,33.88,0.616,0.686,0.702,_textureId);
    drawLine(-22.99,11.51,33.88,-11.43,3.08,33.88);
    drawLine(-32.3,6.7,33.88,-21.29,-2.85,33.88);
    drawLine(-11.43,3.08,33.88,-21.29,-2.85,33.88);

    drawQuad(-36.98,10.77,33.88,-36.98,10.77,20.35,-21.29,-2.85,31.97,-21.29,-2.85,33.88,0.616,0.686,0.702,_textureId);
    drawLine(-21.29,-2.85,31.97,-21.29,-2.85,33.88);
    drawLine(-36.98,10.77,20.35,-21.29,-2.85,31.97);

    drawTriangle(-36.98,10.77,20.35,-21.29,-2.85,31.97,-24.49,2.14,20.35,0.616,0.686,0.702,_textureId);
    drawLine(-21.29,-2.85,31.97,-24.49,2.14,20.35);
    drawLine(-36.98,10.77,20.35,-24.49,2.14,20.35);


    drawQuad(-21.29,-2.85,33.88,-11.43,3.08,33.88,-11.43,3.08,31.97,-21.29,-2.85,31.97,0.616,0.686,0.702,_textureId);
    drawLine(-11.43,3.08,33.88,-11.43,3.08,31.97);
    drawLine(-11.43,3.08,31.97,-21.29,-2.85,31.97);

    drawTriangle(-21.29,-2.85,31.97,-24.49,2.14,20.35,-21.35,2.14,20.35,0.616,0.686,0.702,_textureId);
    drawLine(-21.29,-2.85,31.97,-21.35,2.14,20.35);
    drawLine(-24.49,2.14,20.35,-21.35,2.14,20.35);

    drawTriangle(-21.35,2.14,20.35,-21.35,2.14,23.04,-21.29,-2.85,31.97,0.616,0.686,0.702,_textureId);
    drawLine(-21.35,2.14,20.35,-21.35,2.14,23.04);
    drawLine(-21.35,2.14,23.04,-21.29,-2.85,31.97);

    drawTriangle(-21.29,-2.85,31.97,-11.43,3.08,31.97,-21.35,2.14,23.04,0.616,0.686,0.702,_textureId);
    drawLine(-11.43,3.08,31.97,-21.35,2.14,23.04);

    drawQuad(-11.43,3.08,33.88,-22.96,11.51,33.88,-22.96,11.51,31.97,-11.43,3.08,31.97,0.616,0.686,0.702,_textureId);
    drawLine(-22.96,11.51,31.97,-11.43,3.08,31.97);

    drawTriangle(-22.96,11.51,31.97,-11.43,3.08,31.97,-21.35,2.14,23.04,0.616,0.686,0.702,_textureId);
    drawLine(-21.35,2.14,23.04,-22.96,11.51,31.97);

    drawTriangle(-22.96,11.51,31.97,-21.35,5.54,23.04,-21.35,2.14,23.04,0.616,0.686,0.702,_textureId);
    drawLine(-21.35,5.54,23.04,-21.35,2.14,23.04);
    drawLine(-22.96,11.51,31.97,-21.35,5.54,23.04);

    drawTriangle(-22.96,11.51,31.97,-21.35,5.54,23.04,-22.99,11.62,23.04,0.616,0.686,0.702,_textureId);
    drawLine(-21.35,2.14,23.04,-22.99,11.62,23.04);
    drawLine(-22.99,11.62,33.88,-22.99,11.62,11.68);
    drawLine(-27.14,19.89,33.88,-27.14,19.89,11.68);

    drawQuad(-21.35,2.14,23.04,-21.35,5.54,23.04,-22.99,11.62,23.04,-18.25,4.08,23.04,0.616,0.686,0.702,_textureId);
    drawQuad(-22.99,11.62,23.04,-18.25,4.08,23.04,-10.55,8.93,23.04,-18.68,13.4,23.04,0.616,0.686,0.702,_textureId);

    drawLine(-21.35,2.14,23.04,-10.55,8.93,23.04);
    drawLine(-21.35,2.14,20.35,-10.55,8.93,20.35);
    drawLine(-10.55,8.93,23.04,-18.68,13.4,23.04);
    drawLine(-18.68,13.4,23.04,-22.99,11.62,23.04);

    /////////////////////////////////////////////////////////////////////

    drawTriangle(-10.55,8.93,23.04,-18.68,13.4,23.04,-15.44,19.94,29.23,0.616,0.686,0.702,_textureId);
    drawLine(-10.55,8.93,23.04,-15.44,19.94,29.23);
    drawLine(-18.68,13.4,23.04,-15.44,19.94,29.23);
    drawLine(-10.55,8.93,23.04,-18.68,13.4,23.04);

    drawTriangle(-10.55,8.93,23.04,-15.44,19.94,23.04,-15.44,19.94,29.23,0.616,0.686,0.702,_textureId);
    drawLine(-10.55,8.93,23.04,-15.44,19.94,23.04);
    drawLine(-15.44,19.94,23.04,-15.44,19.94,29.23);

    drawQuad(-12.04,14.74,29.23,-12.04,14.74,23.04,3.44,12.63,23.04,4.79,10.32,31.56,0.616,0.686,0.702,_textureId);
    drawLine(-12.04,14.74,29.23,-12.04,14.74,23.04);
    drawLine(-12.04,14.74,23.04,3.44,12.63,23.04);
    drawLine(3.44,12.63,23.04,4.79,10.32,31.56);
    drawLine(-12.04,14.74,29.23,4.79,10.32,31.56);

    drawQuad(3.44,12.63,23.04,4.79,10.32,31.56,3.57,14.16,29.23,3.57,14.16,23.04,0.616,0.686,0.702,_textureId);
    drawLine(4.79,10.32,31.56,3.57,14.16,29.23);
    drawLine(3.57,14.16,29.23,3.57,14.16,23.04);
    drawLine(3.44,12.63,23.04,3.57,14.16,23.04);

    drawTriangle(-12.04,14.74,29.23,4.79,10.32,31.56,3.57,14.16,29.23,0.616,0.686,0.702,_textureId);

    drawComplexBezier(-12.04,14.74,-17.01,28.46,-17.59,21.17,3.57,14.16,-17.01,28.46,0.5,36.93,29.23,0.616,0.686,0.702,_textureId);

    drawComplexBezier(-37.67,16,-33.89,16,-33.89,16,-37.67,25.89,-29.26,25.89,-33.46,27.98,35.74,.4,.624,.64,_textureIdGlass);
    drawComplexFloatingBezier(-37.67,16,-33.89,16,-33.89,16,-37.67,16,-33.89,16,-33.89,16,35.74,0,.4,.624,.64,_textureIdGlass);
    drawComplexFloatingBezier(-37.67,25.89,-29.26,25.89,-33.46,27.98,-37.67,25.89,-29.26,25.89,-33.46,27.98,35.74,0,.4,.624,.64,_textureIdGlass);
    drawLine(-37.67,16,35.74,-33.89,16,35.74);
    drawLine(-37.67,16,35.74,-37.67,25.89,35.74);
    drawLine(-33.89,16,35.74,-29.26,25.89,35.74);

    drawComplexBezier(-23.59,20.47,-26.15,29.9,-22.39,24.76,-26.57,20.18,-26.15,29.9,-24.49,24.96,29.57,1,1,1,_textureId);
    drawComplexBezier(-37.02,26.22,-33.47,31.29,-36.06,29.94,-37.35,26.06,-34.62,32.33,-37.03,29.95,28.47,1,1,1,_textureId);

    drawComplexBezier(-41.41,26.41,-41.24,13.71,-41.24,13.71,-41.41,26.41,-41.24,13.71,-35.52,17.95,27.36,0.616,0.686,0.702,_textureId);

    drawComplexFloatingBezier(-41.24,13.71,-52.37,40.03,-51.46,13.71,-41.24,13.71,-53.01,34.71,-51.46,13.71,27.36,23.04,0,0,0,_textureId);
    drawComplexFloatingBezier(-41.41,26.41,-52.37,40.03,-43.57,36.2,-41.41,26.41,-53.01,34.71,-43.57,36.2,27.36,23.04,0,0,0,_textureId);
    drawComplexFloatingBezier(-41.24,13.71,-52.37,40.03,-51.46,13.71,-41.41,26.41,-52.37,40.03,-43.57,36.2,27.36,27.36,0,0,0,_textureId);

    drawComplexBezier(-47.02,32.2,-44.64,40.87,-46.52,36.72,-52.72,33.1,-56.06,42.4,-56.06,37.26,23.24,0.4,0.624,0.64,_textureId);
    drawComplexBezier(-44.64,40.87,-56.06,42.4,-56.06,42.4,-44.96,44.2,-56.07,45.19,-51.04,43.1,23.24,0.4,0.624,0.64,_textureId);

    drawComplexBezier(-20.84,12.51,-48.33,11.62,-48.33,11.62,-21.42,37.39,-50.97,39.42,-33.35,41.97,23,.753,.753,.753,_textureId);

    draw3DQuad(-21.42,31.71,23,-20.84,12.51,23,-18.68,13.4,23,-18.68,31.34,23,.753,.753,.753,_textureId);
    draw3DTriangle(-18.68,13.4,-17.01,28.64,-18.68,31.34,23,.753,.753,.753,_textureId);

    drawComplexBezier(-24.23,39.32,-30.59,37.8,-27.42,37.8,-24.23,39.32,-26.15,29.9,-23.43,34.25,30.17,0.4,0.624,0.64,_textureId);
    drawComplexBezier(-26.15,29.9,-30.59,37.8,-30.59,37.8,-26.15,29.9,-35.63,33.26,-31,30.18,30.17,0.4,0.624,0.64,_textureId);
    drawComplexBezier(-35.63,33.26,-41.39,34.26,-38.16,35.01,-30.59,37.8,-41.39,34.26,-38.3,42.36,30.17,0.4,0.624,0.64,_textureId);


    drawComplexFloatingBezier(-15.44,19.94,-17.01,28.46,-16.87,23.5,-18.68,13.4,-18.68,13.4,-18.68,13.4,29.23,23.04,0,0,0,_textureId);
    drawLine(-15.44,19.94,29.23,-18.68,13.4,23.04);
    drawLine(-17.01,28.46,29.23,-18.68,13.4,23.04);

    drawTriangle(-17.01,28.46,29.23,-18.68,13.4,23.04,-17.62,28.17,22.21,0.616,0.686,0.702,_textureId);
    drawLine(-17.01,28.46,29.23,-17.62,28.17,22.21);
    drawLine(-18.68,13.4,23.04,-17.62,28.17,22.21);


    //front gate
    drawTriangle(-21.42,34.67,23.86,-16.9,34.96,23.86,-21.42,46.63,23.86,.3,.3,.3,_textureId);
    drawTriangle(-21.42,34.67,23.04,-16.9,34.96,23.04,-21.42,46.63,23.04,.3,.3,.3,_textureId);
    drawQuad(-21.42,34.67,23.86,-16.9,34.96,23.86,-16.9,34.96,23.04,-21.42,34.67,23.04,.3,.3,.3,_textureId);
    drawQuad(-21.42,34.67,23.86,-21.42,46.63,23.86,-21.42,46.63,23.04,-21.42,34.67,23.04,.3,.3,.3,_textureId);

    drawComplexFloatingBezier(-21.42,37.39,-34.95,40.83,-28.3,40.04,-21.42,46.63,-35.39,45.4,-35.39,45.4,23.86,23.86,0,0,0,_textureId);
    drawComplexFloatingBezier(-21.42,37.39,-34.95,40.83,-28.3,40.04,-21.42,46.63,-35.39,45.4,-35.39,45.4,23.04,23.04,0,0,0,_textureId);
    drawComplexFloatingBezier(-21.42,37.39,-34.95,40.83,-28.3,40.04,-21.42,37.39,-34.95,40.83,-28.3,40.04,23.86,23.04,0,0,0,_textureId);
    drawQuad(-34.95,40.83,23.86,-35.39,45.4,23.86,-35.39,45.4,23.04,-34.95,40.83,23.04,.3,.3,.3,_textureId);

    drawComplexFloatingBezier(-27.42,62.11,-35.39,45.4,-28.54,52.74,-21.42,46.63,-21.42,46.63,-21.42,46.63,23.86,23.86,0,0,0,_textureId);
    drawComplexFloatingBezier(-27.42,62.11,-35.39,45.4,-28.54,52.74,-21.42,46.63,-21.42,46.63,-21.42,46.63,23.04,23.04,0,0,0,_textureId);
    drawComplexFloatingBezier(-27.42,62.11,-35.39,45.4,-28.54,52.74,-27.42,62.11,-35.39,45.4,-28.54,52.74,23.86,23.04,0,0,0,_textureId);
    drawLine(-35.39,45.4,23.86,-35.39,45.4,23.04);


    drawComplexFloatingBezier(-5.08,48.07,-27.42,62.11,-13.84,58.93,-5.08,48.07,-16.9,34.96,-13.08,43.4,23.86,23.86,0,0,0,_textureId);
    drawComplexFloatingBezier(-5.08,48.07,-27.42,62.11,-13.84,58.93,-5.08,48.07,-16.9,34.96,-13.08,43.4,23.04,23.04,0,0,0,_textureId);
    drawComplexFloatingBezier(-5.08,48.07,-27.42,62.11,-13.84,58.93,-5.08,48.07,-27.42,62.11,-13.84,58.93,23.86,23.04,0,0,0,_textureId);
    drawComplexFloatingBezier(-5.08,48.07,-16.9,34.96,-13.08,43.4,-5.08,48.07,-16.9,34.96,-13.08,43.4,23.86,23.04,0,0,0,_textureId);
    drawLine(-5.08,48.07,23.86,-5.08,48.07,23.04);
    drawLine(-27.42,62.11,23.86,-27.42,62.11,23.04);
    drawLine(-16.9,34.96,23.86,-16.9,34.96,23.04);

    draw3DQuad(-8.74,31.55,1.05,-4.99,54.96,1.05,-32.99,55.97,1.05,-33.62,30.02,1.05,0.82,0.71,0.5,_textureIdWood);

    drawBoldLine(-4.99,54.96,1.05,-4.99,54.96,2.59);
    drawBoldLine(-32.99,55.97,1.05,-32.99,55.97,2.56);
    drawBoldLine(-4.99,54.96,2.59,-32.99,55.97,2.56);
    drawBoldLine(-4.99,54.96,2.2,-32.99,55.97,2.17);
    drawBoldLine(-4.99,54.96,1.82,-32.99,55.97,1.81);
    drawBoldLine(-4.99,54.96,1.43,-32.99,55.97,1.43);

    drawBoldLine(-4.99,54.96,2.59,-8.93,31.49,2.59);
    drawBoldLine(-4.99,54.96,2.2,-8.93,31.49,2.2);
    drawBoldLine(-4.99,54.96,1.82,-8.93,31.49,1.82);
    drawBoldLine(-4.99,54.96,1.43,-8.93,31.49,1.43);

    drawBoldLine(-33.38,40.61,2.56,-32.99,55.97,2.56);
    drawBoldLine(-33.38,40.61,2.17,-32.99,55.97,2.17);
    drawBoldLine(-33.38,40.61,1.81,-32.99,55.97,1.81);
    drawBoldLine(-33.38,40.61,1.43,-32.99,55.97,1.43);

    glPushMatrix();{
        GLUquadricObj *quadratic;
        quadratic = gluNewQuadric();
        glTranslatef(-18.13,47.96,1);
        //glColor3f(0.753,0.753,0.753);
        gluCylinder(quadratic,1.7f,1.7f,22.18f,32,32);
        }glPopMatrix();


    //////////////////////////////////////////////////////////////////////////////////

    draw3DTriangle(49.04,-6.2,49.04,3.33,40.54,-1.18,23.11,.4,.624,.64,_textureId);
    drawComplexBezier(49.04,3.33,-17.01,28.46,38.67,39.88,40.54,-1.18,3.57,14.16,27.54,23.32,23.11,.4,.624,.64,_textureId);
    drawComplexBezier(38.99,2.22,3.57,14.16,22.7,2.22,38.99,2.22,3.57,14.16,22.54,23.32,17.95,0.616,0.686,0.702,_textureId);

    draw3DTriangle(49.04,-.01,60.84,-10.32,50.35,16.52,16.02,0.616,0.686,0.702,_textureId);
    draw3DQuad(49.04,-.01,16.02,50.35,16.52,16.02,43.54,23.04,15.85,37.15,23.55,15.85,0.616,0.686,0.702,_textureId);

    drawComplexBezier(67.84,-3.16,60.84,-10.32,65.68,-6.72,67.84,-3.16,50.35,16.52,60.48,7.92,16.02,0.616,0.686,0.702,_textureId);

    drawComplexBezier(108.1,2.87,-2.96,40.55,61.27,28.05,119.54,-17.35,64.62,-4.6,94.85,-5.95,10.8,.4,.624,.64,_textureId);
    drawComplexBezier(119.54,-17.35,108.1,2.87,108.1,2.87,119.54,-17.35,108.1,2.87,119.54,2.87,10.8,.4,.624,.64,_textureId);

    draw3DTriangle(123.71,-8.06,119.54,-17.35,117.54,-8.06,10.8,.4,.624,.64,_textureId);

    drawComplexBezier(10.46,31.65,-2.96,40.55,-2.96,40.55,-8.24,31.71,-2.96,40.55,-6.71,36.8,10.8,.4,.624,.64,_textureId);

    draw3DQuad(131.14,7.22,10.78,118.39,7.22,10.78,118.98,-4.01,10.78,134.15,-4.04,10.78,0.82,0.71,0.5,_textureIdWood);
    draw3DQuad(118.98,-4.01,10.78,134.15,-4.04,10.78,131.63,-14.81,10.78,119.54,-14.81,10.78,0.82,0.71,0.5,_textureIdWood);
    draw3DTriangle(118.43,5.43,119.49,-13.82,108.1,2.87,10.78,0.82,0.71,0.5,_textureIdWood);

    drawQuad(118.43,5.44,10.78,118.43,5.44,9.02,108.1,2.88,9.02,108.1,2.88,10.78,0.3,0.3,0.3,_textureIdWood);
    draw3DTriangle(118.43,5.43,108.1,2.87,113.76,5.46,9.02,0.82,0.71,0.5,_textureIdWood);

    drawComplexFloatingBezier(113.76,5.46,74.63,31,86.67,19.86,108.1,2.87,81.59,14.8,92.42,11.3,9.02,9.02,0,0,0,_textureIdWood);
    drawComplexFloatingBezier(113.76,5.46,74.63,31,86.67,19.86,113.76,5.46,74.63,31,86.67,19.86,9.02,0,0,0,0,_textureIdWood);

    draw3DQuad(81.59,14.8,9.02,74.63,31,9.02,57.49,50.15,0,48.34,44.27,0,0.82,0.71,0.5,_textureIdWood);

    drawComplexBezier(122.35,5.46,93.45,27.27,93.45,27.27,113.76,5.46,74.63,31,86.67,19.86,4.51,.4,.624,.64,_textureIdWood);
    drawComplexBezier(93.45,27.27,76.65,41.63,90,39.47,74.63,31,76.65,41.63,72.91,36.76,4.51,.4,.624,.64,_textureIdWood);


    //BRIDGE

    drawComplexFloatingBezier(70.36,-87.93,155.87,70.49,92.48,3.27,70.36,-87.93,155.87,70.49,92.48,3.27,14.16,12.04,.24,.21,.21,_textureIdRoad);
    drawComplexFloatingBezier(52.46,-63.16,136.15,85.34,82.46,13.79,52.46,-63.16,136.15,85.34,82.46,13.79,14.16,12.04,.24,.21,.21,_textureIdRoad);
    drawComplexFloatingBezier(70.36,-87.93,155.87,70.49,92.48,3.27,52.46,-63.16,136.15,85.34,82.46,13.79,14.16,14.16,.24,.21,.21,_textureIdRoad);
    drawComplexFloatingBezier(70.36,-87.93,155.87,70.49,92.48,3.27,52.46,-63.16,136.15,85.34,82.46,13.79,12.04,12.04,.24,.21,.21,_textureIdRoad);

    draw3DQuadFloat(155.87,70.49,14.16,136.15,85.34,14.16,169.18,137.12,14.16,192.46,122.28,14.16,12.04,.24,.21,.21,_textureIdRoad);

    draw3DQuad(155.96,111.73,12.4,153.57,108.27,12.4,156.17,106.48,12.4,158.67,109.87,12.4,1,0,0,_textureIdRoad);
    draw3DQuad(174.54,98.93,12.4,172.15,95.47,12.4,169.31,97.43,12.4,171.7,100.9,12.4,1,0,0,_textureIdRoad);

    draw3DQuadFloat(156.17,106.48,12.4,158.67,109.87,12.4,171.7,100.9,12.4,169.31,97.43,12.4,9.66,1,0,0,_textureIdRoad);

    drawNew3DQuadFloat(243.52,168.84,14.16,247.21,159.47,14.16,339.43,176.77,6.1,338.33,186.7,5.42,2.12,.24,.21,.21,_textureIdRoad);






    //balchal
    draw3DQuad(132.21,-16.17,52.32,132.07,-16.87,53.35,120.41,-15.41,53.35,120.68,-14.81,53.35,0.82,0.71,0.5,_textureIdWood);
    draw3DTriangle(134.25,-6.24,133.64,-6.21,132.07,-16.87,53.35,0.82,0.71,0.5,_textureIdWood);
    draw3DQuad(135.46,-6.68,52.95,134.85,-6.68,52.95,130.81,9.45,52.95,131.63,9.45,52.95,0.82,0.71,0.5,_textureIdWood);
    draw3DQuad(131.14,8.53,52.95,130.81,9.45,52.95,118.19,9.45,52.95,118.39,8.53,52.95,0.82,0.71,0.5,_textureIdWood);
    draw3DQuad(118.39,8.53,52.95,117.74,8.53,52.95,118.43,5.43,52.95,119.06,5.43,52.95,0.82,0.71,0.5,_textureIdWood);

    glPushMatrix();{
        glTranslatef(122.26,47.43,0);
        glRotatef(50,0,0,1);
        drawBridgeShape(20);
    }glPopMatrix();


    draw3DQuadFloat(127.74,7.67,41.84,126.43,7.83,41.84,125.02,-15.32,41.84,123.72,-15.17,41.84,41.03,1,1,1,_textureId);
    draw3DQuadFloat(129.53,5.73,24.6,128.18,7.54,24.66,128.89,-15.24,23.91,129.53,-15.24,23.91,21.82,1,1,1,_textureId);
    drawNew3DQuadFloat(123.41,-3.77,38.97,122.59,-3.77,38.91,121.78,-14.94,32.06,122.59,-15.04,32.06,0.36,0.2,0.4,1,_textureId);
    drawNew3DQuadFloat(133.53,-6.96,50.61,133.44,-7.62,50.61,124.84,-10.97,41.84,124.79,-11.62,41.84,0.7,0.2,0.4,1,_textureId);
    drawNew3DQuadFloat(131.91,2.41,48.23,131.85,1.66,48.17,125.92,2.67,41.84,125.92,3.43,41.84,0.5,0.2,0.4,1,_textureId);
    drawNew3DQuadFloat(125.22,8.46,50.97,130.08,1.86,46.97,129.63,1.74,46.9,124.36,8.46,50.97,0.36,.2,0.4,1,_textureId);
    drawNew3DQuadFloat(130.08,1.86,46.97,129.63,1.74,46.9,129.34,-9.45,47.48,129.77,-10.04,47.58,0.4,.2,0.4,1,_textureId);
    drawNew3DQuadFloat(123.49,-1.26,37.49,122.96,-1.84,37.39,129.34,-9.45,47.48,129.26,-8.14,46.63,0.5,.2,0.4,1,_textureId);




    drawComplexFloatingBezier(169.18,137.12,187.94,223.12,191.82,188.7,169.18,137.12,187.94,223.12,191.82,188.7,14.16,12.04,.24,.21,.21,_textureIdRoad);
    drawComplexFloatingBezier(183.84,127.77,205.25,226.4,210.42,182.2,183.84,127.77,205.25,226.4,210.42,182.2,14.16,12.04,.24,.21,.21,_textureIdRoad);
    drawComplexFloatingBezier(169.18,137.12,187.94,223.12,191.82,188.7,183.84,127.77,205.25,226.4,210.42,182.2,14.16,14.16,.24,.21,.21,_textureIdRoad);
    drawComplexFloatingBezier(169.18,137.12,187.94,223.12,191.82,188.7,183.84,127.77,205.25,226.4,210.42,182.2,12.04,12.04,.24,.21,.21,_textureIdRoad);

    drawComplexFloatingBezier(183.84,127.77,243.52,168.84,211.12,155.03,183.84,127.77,243.52,168.84,211.12,155.03,14.16,12.04,.24,.21,.21,_textureIdRoad);
    drawComplexFloatingBezier(192.46,122.28,247.21,159.47,216.26,146.6,192.46,122.28,247.21,159.47,216.26,146.6,14.16,12.04,.24,.21,.21,_textureIdRoad);
    drawComplexFloatingBezier(183.84,127.77,243.52,168.84,211.12,155.03,192.46,122.28,247.21,159.47,216.26,146.6,14.16,14.16,.24,.21,.21,_textureIdRoad);
    drawComplexFloatingBezier(183.84,127.77,243.52,168.84,211.12,155.03,192.46,122.28,247.21,159.47,216.26,146.6,12.04,12.04,.24,.21,.21,_textureIdRoad);


    drawNew3DQuadFloat(187.94,223.12,14.16,205.25,226.4,14.16,204.21,235.28,13.01,187,233.76,13.03,2.12,.24,.21,.21,_textureIdRoad);


////////////////////////////////////////////
    drawQuad(-12.17,30.46,23.11,-12.17,30.96,0,-17.01,28.46,0,-17.01,28.46,23.11,1,1,1,_textureIdGlass);
    drawQuad(-16.71,28.46,0,-16.71,28.46,23.11,-18.68,31.34,23.04,-18.68,31.34,0,1,1,1,_textureIdGlass);
    drawQuad(-18.68,31.4,23.04,-18.68,31.4,0,-21.42,31.77,0,-21.42,31.77,23.04,1,1,1,_textureIdGlass);
    drawQuad(-21.25,31.77,0,-21.25,31.77,23.00,-21.42,37.4,23.00,-21.42,37.4,0,1,1,1,_textureIdGlass);

    drawQuad(49.1,-5.15,21.54,49.1,-4.21,16.73,49.1,3.06,16.73,49.1,0.34,21.67,1,1,1,_textureIdGlass);


}




void show2DScene()
{
    glColor3f(1.0f, 0.0f, 0.0f);
    float x = screen_width - 350;
    drawText("PRESS h TO HIDE/SHOW THIS HELP!",MEDIUM_TEXT,x, 620);
    drawText("*******************************",MEDIUM_TEXT,x, 600);
    drawText("Forward : f Backward : b",MEDIUM_TEXT,x, 580);
    drawText("Up : o Down : i",MEDIUM_TEXT,x, 560);
    drawText("Right : r Left : l",MEDIUM_TEXT,x, 540);
    drawText("RotateX (x,y)",MEDIUM_TEXT,x, 520);
    drawText("RotateY (a,d)",MEDIUM_TEXT,x, 500);
    drawText("RotateZ (m,n)",MEDIUM_TEXT,x, 480);
    drawText("1(Daylight)-6(Nightmode) Light Color Change",MEDIUM_TEXT,x, 460);
    drawText("7 TO ENABLE UPPER GLOBAL LIGHT",MEDIUM_TEXT,x, 440);
    drawText("*******************************",MEDIUM_TEXT,x, 420);
    drawText("PRESS q OR Escape TO QUIT.",MEDIUM_TEXT,x, 400);
}

//3d mode init
void initModes(float width,float height)
{
    /* tell OpenGL to use the whole window for drawing */
    glViewport(0, 0, (GLsizei) width, (GLsizei) height);
    //for setting up camera
    glMatrixMode( GL_PROJECTION );
    //perspective setup
    //angle of view frustum
    glLoadIdentity();
    float angle = 70;
    float aspectRatio = width/height;
    float nearPlaneDistance = 0.1;
    float farPlaneDistance = 10000;
    gluPerspective( angle,aspectRatio,nearPlaneDistance,farPlaneDistance);
    //    gluOrtho2D(0, width, 0, height);
    //get back to drawing/transformation mode
    glMatrixMode(GL_MODELVIEW);
}

/* Called when window is resized,
 also when window is first created,
 before the first call to display(). */
void reshape(int w, int h)
{
    /* save new screen dimensions */
    screen_width = (GLdouble) w;
    screen_height = (GLdouble) h;
    initModes(screen_width,screen_height);
    
}

void Display(){

    //clear the display
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    if(isNightMode)
    {
        glClearColor(BLACK, 0); //color black
    }else{
        
        glClearColor(WHITE, 0); //color black
    }
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    /********************
    / set-up camera here
    ********************/
    //load the correct matrix -- MODEL-VIEW matrix
    glMatrixMode(GL_MODELVIEW);

    //initialize the matrix
    glLoadIdentity();

    Camera.Render();

    float pX = 50;
    float pY = 200;
    float pZ = 60;
    float lX = 0;
    float lY = 0;
    float lZ = 0;
    float upX = 0;
    float upY = 0;
    float upZ = 1;
    gluLookAt(pX,pY,pZ,lX,lY,lZ,upX,upY,upZ);
    glMatrixMode(GL_MODELVIEW);

    //fixed global light
//    glLightfv(GL_LIGHT1, GL_AMBIENT,light_ambient_orange);
    //dynamic lighting
    
    switch (light_color_choice) {
        
        case 1:
            glLightfv(GL_LIGHT0, GL_AMBIENT,  light_ambient_white);
            break;
        case 2:
            glLightfv(GL_LIGHT0, GL_AMBIENT,  light_ambient_orange);
            break;
        case 3:
            glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient_cyan);
            break;
        case 4:
            glLightfv(GL_LIGHT0, GL_AMBIENT,  light_ambient_green);
            break;
        case 5:
            glLightfv(GL_LIGHT0, GL_AMBIENT,  light_ambient_red);
            break;
        case 6:
            glLightfv(GL_LIGHT0, GL_AMBIENT,  light_ambient_black);
            break;
    }
    //update light positions
    light_position[0]=LightX;
    light_position[1]=LightY;
    light_position[2]=LightZ;
    //common settings for light0 and 1
    glLightfv(GL_LIGHT0, GL_DIFFUSE,  light_diffuse);
    glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
    glLightfv(GL_LIGHT0, GL_POSITION, light_position);
    
    if(globalLightOn)
    {
        glEnable(GL_LIGHT1);
        glLightfv(GL_LIGHT1, GL_DIFFUSE,  light_diffuse2);
        glLightfv(GL_LIGHT1, GL_SPECULAR, light_specular2);
        glLightfv(GL_LIGHT1, GL_POSITION, light_position2);
    }else{
        glDisable(GL_LIGHT1);
    }

    //WILL draw grid IF the "canDrawGrid" is true:

    if(canDrawGrid){
        glColor3f(0.5, 0.5, 0.5); //grey
        glBegin(GL_LINES);{
            for(int i=-30;i<=30;i++){

                if(i==0)
                    continue; //SKIP the MAIN axes

                //lines parallel to Y-axis
                glVertex3f(i*10, -300, 0);
                glVertex3f(i*10,  300, 0);

                //lines parallel to X-axis
                glVertex3f(-300, i*10, 0);
                glVertex3f( 300, i*10, 0);
            }
        }glEnd();
    }

    // draw the two AXES
    glColor3f(1, 1, 1); //100% white
    glBegin(GL_LINES);{
        //Y axis
        glVertex3f(0, -300, 0); // intentionally extended to -150 to 150, no big deal
        glVertex3f(0,  300, 0);

        //X axis
        glVertex3f(-300, 0, 0);
        glVertex3f( 300, 0, 0);
    }glEnd();
    //show manual
    if(showHelp)
    {
        glMatrixMode(GL_PROJECTION);
        glPushMatrix();
        glLoadIdentity();
        gluOrtho2D(0.0,screen_width, 0.0, screen_height);
        glMatrixMode(GL_MODELVIEW);
        glPushMatrix();
        glLoadIdentity();
        
        show2DScene();
        
        glMatrixMode(GL_MODELVIEW);
        glPopMatrix();
        glMatrixMode(GL_PROJECTION);
        glPopMatrix();
        glEnable(GL_TEXTURE_2D);
        //BACK TO 3DSPACE
        initModes(screen_width, screen_height);
    }
    //*******************************CODE STARTS****************************************

    totalScene();
    //drawScene();
    glFlush();
	glutSwapBuffers();
}





void animate(){
    //codes for any changes in Camera

    cameraAngle+=cameraAngleVar;    // camera will rotate at 0.002 radians per frame.       // keep the camera steady NOW!!

        //codes for any changes in Models

        rectAngle += 20;


    //MISSING SOMETHING? -- YES: add the following
    glutPostRedisplay(); //this will call the display AGAIN
}

void specialKeyListener(int key, int x,int y){
    switch(key){
        case GLUT_KEY_LEFT:
            LightX +=5;
            break;
        case GLUT_KEY_RIGHT:
            LightX -=5;
            break;
        case GLUT_KEY_UP:
            LightY +=5;
            break;
        case GLUT_KEY_DOWN:
            LightY -=5;
            break;
            
    }
}


void KeyDown(unsigned char key, int x, int y)
{
	switch (key)
	{
        case '1':
            light_color_choice = 1;
            isNightMode = false;
            break;
        case '2':
            light_color_choice = 2;
            break;
        case '3':
            light_color_choice = 3;
            break;
        case '4':
            light_color_choice = 4;
            break;
        case '5':
            light_color_choice = 5;
            break;
        case '6':
            light_color_choice = 6;
            isNightMode = true;
            break;
        case '7':
            globalLightOn = !globalLightOn;
            break;
        case 27:		//ESC
            exit(0);
            break;
        case 'q':		//quit
            exit(0);
            break;
        case 'h':		//quit
            showHelp = !showHelp;
            break;
        case 'a':
            Camera.RotateY(5.0);
            Display();
            break;
        case 'd':
            Camera.RotateY(-5.0);
            Display();
            break;
        case 'f':
            Camera.MoveForward( -5.0 ) ;
            Display();
            break;
        case 'b':
            Camera.MoveForward( 5.0 ) ;
            Display();
            break;
        case 'x':
            Camera.RotateX(5.0);
            Display();
            break;
        case 'y':
            Camera.RotateX(-5.0);
            Display();
            break;
        case 'l':
            Camera.StrafeRight(-5.0);
            Display();
            break;
        case 'r':
            Camera.StrafeRight(5.0);
            Display();
            break;
        case 'i':
            Camera.MoveUpward(-5.0);
            Display();
            break;
        case 'o':
            Camera.MoveUpward(5.0);
            Display();
            break;

        case 'm':
            Camera.RotateZ(-5.0);
            Display();
            break;
        case 'n':
            Camera.RotateZ(5.0);
            Display();
            break;

	}
}



void mouseListener(int button, int state, int x, int y){ //x, y is the x-y of the screen (2D)
    switch(button){
    case GLUT_LEFT_BUTTON:
    if(state == GLUT_DOWN){ // 2 times?? in ONE click? -- solution is checking DOWN or UP
        cameraAngleDelta = -cameraAngleDelta;
    }
    break;

    case GLUT_RIGHT_BUTTON:
    //........
    break;

    case GLUT_MIDDLE_BUTTON:
    //........
    break;

    default:
    break;
    }
}





void init(){
    cameraAngleVar=0.000;   //// init the cameraAngle
    rectAngle = 0;

    CLX=CLY=CLZ=0;
    CPX=120;
    CPY=-20;
    CPZ=200;
    //clear the screen
    glClearColor(BLACK, 0);

    //set up 3d projection space
    initModes(screen_width, screen_height);

    _textureId = LoadTexture("new.bmp");
    _textureIdRoad = LoadTexture("road.bmp");
    _textureIdWood = LoadTexture("wood.bmp");
    _textureIdGlass =LoadTexture("glass.bmp");
    _textureIdTest= LoadTexture("test.bmp");
}


int main(int argc, char **argv){

    showHelp = true;
    screen_width = 1024;
    screen_height = 680;
    glutInit(&argc,argv);
    glutInitWindowSize(screen_width, screen_height);
    glutInitWindowPosition(10,10);
    glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGB); //Depth, Double buffer, RGB color    
    glutCreateWindow("Lighting and Texturing");
   
    
    //init the textures & matrices
    init();
    //draw grids? true
    canDrawGrid = true;
    //enable Depth Testing
    glEnable(GL_DEPTH_TEST); 
    
    //lighting
    //ambient-providing a bit of visibility when there is darkness
//    glLightModelfv(GL_LIGHT_MODEL_AMBIENT, light_ambient_cyan_light);
    //source1
    glEnable(GL_LIGHT0);
    glEnable(GL_NORMALIZE);
    glEnable(GL_LIGHTING);
    
    //material for realistic lighting
    glEnable(GL_COLOR_MATERIAL);
    glColorMaterial(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE );
    glMaterialfv(GL_FRONT, GL_AMBIENT,   mat_ambient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE,   mat_diffuse);
    glMaterialfv(GL_FRONT, GL_SPECULAR,  mat_specular);
    glMaterialfv(GL_FRONT, GL_SHININESS, high_shininess);
    //init camera
    Camera.Move( F3dVector(0.0, 0.0, 3.0 ));
    Camera.MoveForward( 1.0 );

    

    glutDisplayFunc(Display); //display callback function
    glutIdleFunc(animate); //what you want to do in the idle time (when no drawing is occuring)

    /* register function to handle window resizes */
    glutReshapeFunc(reshape);


    glutKeyboardFunc(KeyDown);
    glutSpecialFunc(specialKeyListener);

    //ADD mouse listeners:
    glutMouseFunc(mouseListener);

    glutMainLoop(); //The main loop of OpenGL

    return 0;
}
