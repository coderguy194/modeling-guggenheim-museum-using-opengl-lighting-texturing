#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#ifndef IMPORTS_H_INCLUDED
#define IMPORTS_H_INCLUDED
#include <math.h>
#include "VectorHelper.h"
#define MEDIUM_TEXT 1
#define SMALL_TEXT 2
#define LARGE_TEXT 3
#define THREE_D 2

//global vars for screen size
static int screen_width,screen_height;


#endif // IMPORTS_H_INCLUDED
