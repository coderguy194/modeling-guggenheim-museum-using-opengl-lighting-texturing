//
//  VectorHelper.cpp
//  TexturingLighting
//
//  Created by Md. Abdul Munim Dibosh on 3/8/14.
//  Copyright (c) 2014 Md. Abdul Munim Dibosh. All rights reserved.
//

#include "VectorHelper.h"

Vertex Vertex3d(float x, float y, float z )
{
	Vertex tmp;
	tmp.x = x;
	tmp.y = y;
	tmp.z = z;
	return tmp;
}

Polygon polygon(Vertex array[],int verticesCount)
{
    Polygon p;
    p.no_of_vertices = verticesCount;
    p.vertices = (Vertex*) malloc(p.no_of_vertices*sizeof(Vertex));
    for(int i=0;i<verticesCount;i++)
    {
        //taking a copy not the exact pointer to that object
        p.vertices[i] = Vertex3d(array[i].x, array[i].y, array[i].z);
    }
    
    return p;
}

Vertex getNormalOfAPolygonalSurface(Polygon p)
{
    Vertex normal = Vertex3d(0, 0, 0);
    
    for(int i=0;i<p.no_of_vertices;i++)
    {
        Vertex current = p.vertices[i];
        Vertex next = p.vertices[(i+1)%p.no_of_vertices];
        normal.x += (current.y-next.y)*(current.z+next.z);
        normal.y += (current.z-next.z)*(current.x+next.x);
        normal.z += (current.x-next.x)*(current.y+next.y);
    }
    //normalize it
    float length = sqrtf(normal.x*normal.x + normal.y*normal.y + normal.z*normal.z);
    normal.x = normal.x/length;
    normal.y = normal.y/length;
    normal.z = normal.z/length;
    
    return normal;
}